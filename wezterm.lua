-- Pull in the wezterm API
local wezterm = require("wezterm")

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
	config = wezterm.config_builder()
end

-- This is where you actually apply your config choices
config.window_frame = {
	-- The font used in the tab bar.
	-- Roboto Bold is the default; this font is bundled
	-- with wezterm.
	-- Whatever font is selected here, it will have the
	-- main font setting appended to it to pick up any
	-- fallback fonts you may have used there.
	font = wezterm.font({ family = "sf-mono", weight = "Bold" }),
	-- The size of the font in the tab bar.
	-- Default to 10.0 on Windows but 12.0 on other systems
	font_size = 12.0,
	-- The overall background color of the tab bar when
	-- the window is focused
	active_titlebar_bg = "#333333",
	-- The overall background color of the tab bar when
	-- the window is not focused
	inactive_titlebar_bg = "#333333",
}

-- Cursor config
config.default_cursor_style = "BlinkingBar"
config.animation_fps = 60

-- Disable check for updates
config.check_for_updates = false

-- Disable in terminal notification(It was quite annoying)
config.show_update_window = false

-- Hide tabs if only a single window is open
config.hide_tab_bar_if_only_one_tab = true

-- Window decoration options
-- window_decorations = "NONE" - disables titlebar and border (borderless mode), but causes problems with resizing and minimizing the window, so you probably want to use RESIZE instead of NONE if you just want to remove the title bar.
-- window_decorations = "TITLE" - disable the resizable border and enable only the title bar
-- window_decorations = "RESIZE" - disable the title bar but enable the resizable border
-- window_decorations = "TITLE | RESIZE" - Enable titlebar and border. This is the default
config.window_decorations = "RESIZE"

-- opacity of background
config.window_background_opacity = 0.6
-- For example, changing the color scheme:
config.color_scheme = "Catpuccin Latte"

config.tab_bar_at_bottom = true

-- config.colors = {
-- 	-- The default text color
-- 	foreground = "#dddddd",
-- 	-- The default background color
-- 	background = "black",
-- 	-- cursor and the cursor style is set to Block
-- 	cursor_bg = "white",
-- 	-- Overrides the text color when the current cell is occupied by the cursor
-- 	cursor_fg = "white",
-- 	-- Specifies the border color of the cursor when the cursor style is set to Block,
-- 	-- or the color of the vertical or horizontal bar when the cursor style is set to
-- 	-- Bar or Underline.
-- 	cursor_border = "#52ad70",
-- 	-- the foreground color of selected text
-- 	selection_fg = "black",
-- 	-- the background color of selected text
-- 	selection_bg = "#fffacd",
-- 	-- The color of the scrollbar "thumb"; the portion that represents the current viewport
-- 	scrollbar_thumb = "#222222",
-- 	-- The color of the split lines between panes
-- 	split = "#444444",
-- 	ansi = {
-- 		"black",
-- 		"maroon",
-- 		"green",
-- 		"olive",
-- 		"navy",
-- 		"purple",
-- 		"teal",
-- 		"silver",
-- 	},
-- 	brights = {
-- 		"grey",
-- 		"red",
-- 		"lime",
-- 		"yellow",
-- 		"blue",
-- 		"fuchsia",
-- 		"aqua",
-- 		"white",
-- 	},
-- 	-- Arbitrary colors of the palette in the range from 16 to 255
-- 	indexed = { [136] = "#af8700" },
-- 	-- Since: 20220319-142410-0fcdea07
-- 	-- When the IME, a dead key or a leader key are being processed and are effectively
-- 	-- holding input pending the result of input composition, change the cursor
-- 	-- to this color to give a visual cue about the compose state.
-- 	compose_cursor = "orange",
-- 	-- Colors for copy_mode and quick_select
-- 	-- available since: 20220807-113146-c2fee766
-- 	-- In copy_mode, the color of the active text is:
-- 	-- 1. copy_mode_active_highlight_* if additional text was selected using the mouse
-- 	-- 2. selection_* otherwise
-- 	copy_mode_active_highlight_bg = { Color = "#000000" },
-- 	-- use `AnsiColor` to specify one of the ansi color palette values
-- 	-- (index 0-15) using one of the names "Black", "Maroon", "Green",
-- 	--  "Olive", "Navy", "Purple", "Teal", "Silver", "Grey", "Red", "Lime",
-- 	-- "Yellow", "Blue", "Fuchsia", "Aqua" or "White".
-- 	copy_mode_active_highlight_fg = { AnsiColor = "Black" },
-- 	copy_mode_inactive_highlight_bg = { Color = "#52ad70" },
-- 	copy_mode_inactive_highlight_fg = { AnsiColor = "White" },
-- 	quick_select_label_bg = { Color = "peru" },
-- 	quick_select_label_fg = { Color = "#ffffff" },
-- 	quick_select_match_bg = { AnsiColor = "Navy" },
-- 	quick_select_match_fg = { Color = "#ffffff" },
-- }

-- and finally, return the configuration to wezterm
return config
